# Belajar Git

## Tugas Implementasi
1. **Fork** repository ini
2. Buat ***branch baru*** berdasarkan branch **master** 
3. Silakan buat tiga perubahan(**3 buah commit**) pada branch tersebut
4. Lakukan **Pull Request** perubahan ke branch **master**